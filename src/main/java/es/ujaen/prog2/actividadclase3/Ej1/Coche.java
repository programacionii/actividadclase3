/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.actividadclase3.Ej1;

/**
 *
 * @author admin
 */
public class Coche {
    
    Motor motor;
    Rueda ruedas[];
    Puerta puertaPiloto;
    Puerta puertaCopiloto;
    
    public void arrancarMotor(){
        motor.encenderMotor();
    }
    
    public void subirVentanilla(boolean puertaPiloto){
        if(puertaPiloto)
            this.puertaPiloto.subirVentanilla();
        else
            puertaCopiloto.subirVentanilla();
    }
    
    public void bajarVentanilla(boolean puertaPiloto){
        if(puertaPiloto)
            this.puertaPiloto.bajarVentanilla();
        else
            puertaCopiloto.bajarVentanilla();
    }
       
    
}
