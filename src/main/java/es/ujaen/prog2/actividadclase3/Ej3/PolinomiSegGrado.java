/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.actividadclase3.Ej3;

/**
 *
 * @author admin
 */
public class PolinomiSegGrado {

    private float a, b, c;

    public PolinomiSegGrado() {
        a = 0;
        b = 0;
        c = 0;
    }

    public PolinomiSegGrado(float _a, float _b, float _c) {
        a = _a;
        b = _b;
        c = _c;
    }

    public PolinomiSegGrado suma(PolinomiSegGrado derecho) {

        float auxA = a + derecho.a;
        float auxB = b + derecho.b;
        float auxC = c + derecho.c;

        PolinomiSegGrado resultado = new PolinomiSegGrado(auxA, auxB, auxC);

        return resultado;

    }

    public PolinomiSegGrado resta(PolinomiSegGrado derecho) {

        float auxA = a - derecho.a;
        float auxB = b - derecho.b;
        float auxC = c - derecho.c;

        PolinomiSegGrado resultado = new PolinomiSegGrado(auxA, auxB, auxC);

        return resultado;

    }

}
