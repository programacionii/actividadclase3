/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.actividadclase3.Ej1;

/**
 *
 * @author admin
 */
public class Rueda {

    float presionMáxima;
    float presionActual;
    
    boolean inflarRueda(float presionDeseada){
        if(presionDeseada < presionActual)
            return false;
        
        if(presionDeseada > presionMáxima)
            return false;
        
        presionActual = presionDeseada;
        return true;
    }
        
    boolean desinflarRueda(float presionDeseada){
        if(presionDeseada > presionActual)
            return false;
        
        if(presionDeseada < 0)
            return false;
        
        presionActual = presionDeseada;
        return true;
    }
    
}
