/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.actividadclase3.Ej4;

/**
 *
 * @author admin
 */
public class Rectangulo {

    float esqII[];
    float esqID[];
    float esqSI[];
    float esqSD[];

    public Rectangulo() {

        esqII = new float[2];
        esqII[0] = 0;
        esqII[1] = 0;
        esqID = new float[2];
        esqID[0] = 0;
        esqID[1] = 0;
        esqSI = new float[2];
        esqSI[0] = 0;
        esqSI[1] = 0;
        esqSD = new float[2];
        esqSD[0] = 0;
        esqSD[1] = 0;

    }

    public Rectangulo(float[] esqII, float[] esqID, float[] esqSI, float[] esqSD) {
        this.esqII = esqII;
        this.esqID = esqID;
        this.esqSI = esqSI;
        this.esqSD = esqSD;
    }

    public Rectangulo(float ancho, float alto) {

        esqII = new float[2];
        esqII[0] = 0;
        esqII[1] = 0;
        esqID = new float[2];
        esqID[0] = ancho;
        esqID[1] = 0;
        esqSI = new float[2];
        esqSI[0] = 0;
        esqSI[1] = alto;
        esqSD = new float[2];
        esqSD[0] = ancho;
        esqSD[1] = alto;

    }

    public float superficie() {
        float ancho = esqID[0] - esqII[0];
        float alto = esqSI[1] - esqII[1];

        return ancho * alto;
    }

    public void desplazar(float x, float y) {

        esqII[0] = esqII[0] + x;
        esqID[0] = esqID[0] + x;
        esqSI[0] = esqSI[0] + x;
        esqSD[0] = esqSD[0] + x;

        esqII[1] = esqII[1] + y;
        esqID[1] = esqID[1] + y;
        esqSI[1] = esqSI[1] + y;
        esqSD[1] = esqSD[1] + y;
        
    }

}
